import * as Api from '../api/Api';

export async function get(req) {
    await Api.Auth.logout(req.locals.sess).catch(() => {});
    req.locals.sess = "";
    return {
        status: 302,
        headers: {
            location: '/'
        }
    }
}