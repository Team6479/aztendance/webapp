export async function get(req) {
    req.locals.sess = req.query.get('sess');
    return {
        status: 302,
        headers: {
            location: req.query.get('to')
        }
    }
}