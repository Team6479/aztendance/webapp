import cookie from 'cookie';

export async function handle({request, resolve}) {
    const cookies = cookie.parse(request.headers.cookie || '');
    request.locals.sess = cookies.sess;
    const response = await resolve(request);
    const expires = new Date((new Date()).getTime() + 7*24*60*60*1000);
    response.headers['set-cookie'] = `sess=${request.locals.sess || ''}; Path=/; HttpOnly; SameSite=Lax; Expires=${expires.toUTCString()}`;
    return response;
}

export async function getSession(request) {
    return {
      sess: request.locals.sess
    }
  }