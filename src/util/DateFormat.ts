const pad2 = (n) => {
    const s = "00" + n;
    return s.substring(s.length - 2);
}

const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
export const formatDate = (d: Date) => {
    return d.getDate() + " " + months[d.getMonth()] + " " + d.getFullYear()
        + " @ " + pad2(d.getHours()) + ":" + pad2(d.getMinutes());
}

export const formatDuration = (s: number) => {
    const h = Math.floor(s / 3600); 
    s -= h * 3600;
    const m = Math.floor(s / 60);
    s -= m * 60;
    return pad2(h) + ":" + pad2(m) + ":" + pad2(s);
}