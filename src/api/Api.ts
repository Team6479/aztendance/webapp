export interface SessionInfo {
    sessionInfo: {
        user: string
        start: number
        expires: number
    }
    sessionID: string
}

export interface MeetingInfo {
    meetingID: string
    meetingCode: string
    start: number
    loc: string
}

export interface StateInfo {
    userID: string,
    meetingID: string,
    since: number
}

export interface UserInfo {
    usr: string
    name: string
    sudo: boolean
}

export interface LogInfo {
    id: string
    loc: string
    joined: number
    left: number
}

import axios from 'axios';
import qs from 'qs';

const baseUrl = "https://aztendance.team6479.org";

export class Auth {

    static async login(usr: string, pwd: string): Promise<SessionInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/auth/login", qs.stringify({
                usr: usr,
                pwd: pwd
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as SessionInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async logout(sess: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/auth/logout", qs.stringify({
                sess: sess
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async check(sess: string): Promise<SessionInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/auth/check", qs.stringify({
                sess: sess
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as SessionInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

}

export class Meeting {

    static async create(sess: string, loc: string): Promise<MeetingInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/meeting/create", qs.stringify({
                sess: sess,
                loc: loc
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as MeetingInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async end(sess: string, id: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/meeting/end", qs.stringify({
                sess: sess,
                id: id
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async get(sess: string, id: string, code?: string): Promise<MeetingInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/meeting/get", qs.stringify({
                sess: sess,
                id: id,
                code: code || ""
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as MeetingInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async join(sess: string, id: string, code: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/meeting/join", qs.stringify({
                sess: sess,
                id: id,
                code: code
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async leave(sess: string, id: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/meeting/leave", qs.stringify({
                sess: sess,
                id: id
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

}

export class State {
    static async get(sess: string, usr: string): Promise<StateInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/state/get", qs.stringify({
                sess: sess,
                usr: usr
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as StateInfo)
            }).catch(error => {
                reject(error);
            })
        })
    }
}

export class User {
    
    static async create(sess: string, usr: string, name: string, pwd: string, sudo: number): Promise<UserInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/create", qs.stringify({
                sess: sess,
                usr: usr,
                name: name,
                pwd: pwd,
                sudo: sudo
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as UserInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async get(sess: string, usr: string): Promise<UserInfo> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/get", qs.stringify({
                sess: sess,
                usr: usr
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as UserInfo);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async chsudo(sess: string, usr: string, sudo: number): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/chsudo", qs.stringify({
                sess: sess,
                usr: usr,
                sudo: sudo
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async passwd(sess: string, usr: string, pwd: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/passwd", qs.stringify({
                sess: sess,
                usr: usr,
                pwd: pwd
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async rename(sess: string, usr: string, name: string): Promise<void> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/rename", qs.stringify({
                sess: sess,
                usr: usr,
                name: name
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(() => {
                resolve();
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async list(sess: string): Promise<Array<UserInfo>> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/list", qs.stringify({
                sess: sess
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as Array<UserInfo>);
            }).catch(error => {
                reject(error);
            })
        })
    }

    static async log(sess: string, usr: string): Promise<Array<LogInfo>> {
        return new Promise((resolve, reject) => {
            axios.post(baseUrl + "/api/user/log", qs.stringify({
                sess: sess,
                usr: usr
            }), {headers: {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}}
            ).then(response => {
                resolve(response.data as Array<LogInfo>);
            }).catch(error => {
                reject(error);
            })
        })
    }
}
